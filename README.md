# PipetBot-A8X

PipetBot-A8Xtend


## Copyright notice

Copyright (c) 2018 DerAndere

This software, excluding third-party software components, is licensed under 
the terms of the MIT License (https://opensource.org/licenses/MIT), 
see ./LICENSE. Contents of the directories ./documentation/ 
and ./src/[Project name]/resources is dual-licensed under the terms of the MIT 
License or the Creative Commons Attribution 4.0 license (CC BY 4.0): 
https://creativecommons.org/licenses/by/4.0/legalcode, see 
./documentation/LICENSE and ./src/[Project name]/resources/LICENSE. 
For all third-party components incorporated into this Software, those 
components are licensed under the original license provided by the owner 
of the applicable component (see ./LICENSE file). Source code of this software 
is linked with third-party libraries that are licensed under the original 
license provided by the owner of the applicable library (see 
./LICENSE file and comments in the source code).
If applicable, third-party components are kept in separate child 
directories ./[Project name]/dep/[component name]. Please see the file 
./src/[Project name]/dep/[component name]/LICENSE (and the file
./src/[Project name]/dep/[component name]/NOTICE file), if provided, in each 
third-party folder, or otherwise see comments in the source code.

// SPDX-License-Identifier: MIT