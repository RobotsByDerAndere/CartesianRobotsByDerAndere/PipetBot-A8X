/**
 * @title: PipetBot_A8Xtend.h
 * @version: 6.0
 * @author: DerAndere
 * @created: 2018
 * // SPDX-License-Identifier: MIT
 * //Copyright � 2018 DerAndere
 * @license: MIT
 * @language: C++
 * @info: https://it-by-derandere.blogspot.com/programming
 * @about: This sketch can be uploaded on an Atmega328 to receive commands or
 * data encoded as Servo-PWM signals via its pin PB2 (digital pin 8 of an
 * Arduino Uno Rev.3 development board)
 * The first PWM pulse burst with a period of 20 ms should last for
 * 200 ms and encodes  the type of command. It should be followed by a 300 ms
 * burst with a period of 20 ms and a pulse width of > 19950 us high (3.3-5 mV).
 * The following burst of pulses with a period of 20 ms encodes a parameter value
 * and should last for 300 ms followed by a 300 ms burst with a period of 20 ms
 * and a pulse width of > 19950 us.
 * When no command is to be transmitted, the voltage at the pin PB2 (digital
 * pin 8 of the Arduino Uno Rev.3) should be constantly 0mV (state "low") or
 * a Servo-PWM signal with a period of 20 ms and a pulse width of either
 * < 300 us or > 19950 us.
 * Adapted from "Measuring a duty cycle using the input capture unit"
 * (http://www.gammon.com.au/forum/?id=11504&reply=13#reply13, Copyright: Nick
 * Gammon 2015, Creative Commons Attribution 3.0 Australia License. The averaging
 * algorithm is from https://geekshavefeelings.com/posts/integer-arithmetic-continued
 * https://geekshavefeelings.com/posts/integer-arithmetic-continued (Copyright
 * (c) Xo Wang 2013) with permission from Xo Wang.
 * That averaging algorithm is a generalization of the solution mentioned in
 * US-patents US08748851 (1996) and US6007232A (1998) by Roney S. Wong
 * (Samsung Electronics Co Ltd). I have no expertise in judging if the usage of
 * this code in a final product, even if non-commercial, is allowed.
 * Use at your own responsibility.
 * Adapted by Author: DerAndere
 * @language: C/C++11 for the Microchip AVR toolchain based on g++ from the
 * GNU Compiler Collection
 * @launch target: Microchip Atmega328.
 */


#ifndef _PipetBot_A8Xtend_H_
#define _PipetBot_A8Xtend_H_
#include "Arduino.h"

#include <Arduino.h> // Available after installing the Arduino IDE https://www.arduino.cc/en/Main/Software
#include "ServoTimer2.h" // https://github.com/nabontra/ServoTimer2

// Input: Pin D8
# ifndef F_CPU
# define F_CPU 16000000UL
# endif //F_CPU
const int icpPin = { 8 };
const int servo0Pin = { 4 };
const int servo1Pin = { 7 };
const int maxPulseReads = { 3 };
const uint32_t pwmSustainMillis = { 300 };
static ServoTimer2 servo0;
static ServoTimer2 servo1;
static uint8_t mode = { 1 };
static uint32_t currentMillis = { 0 };
static uint32_t previousMillis = { 0 };
static volatile uint32_t overflowCountAtFlanks[2*maxPulseReads] = { 0 };
static volatile uint32_t overflowCount = { 0 };
static volatile uint32_t timer1CounterValueAtFlanks[2*maxPulseReads] = { 0 };
static volatile int index = { 0 };
static unsigned int averagePulseWidthHighMicros = { 65535 };
static unsigned int pulseWidthHighMicros1 = { 65535 };
static unsigned int pulseWidthHighMicros2 = { 65535 };
const unsigned int maxServoPWMhighMicros = { 2250 };

// 16 bit timer1 overflows every 2^16 = 65536 counts. Count the overflows to calculate long time periods unsigned type for handling of rollover by unsigned integer arithmetics
ISR (TIMER1_OVF_vect);

/**
 * Input Capture at the ICR pin (PB2 of the Atmega328, digital pin 8 of the Arduino
 * Uno Rev.3) is the best approach measuring pulses with pulse widths below 50ms. If
 * a change from 0 V at The 16 bit timer1 value in the TCNT1 register is stored in
 * the ICR1 register.
 */
ISR (TIMER1_CAPT_vect);

/**
 * Overflow-proof average of two signed integers. The naive average function is
 * erroneous when the sum of the inputs overflows integer limits; this average
 * works by summing the halves of the input values and then correcting the sum
 * for rounding.
 * @param a first value, as a 32-bit (un)signed integer
 * @param b second value, as a 32-bit (un)signed integer
 * @return signed average of the two values, rounded towards zero if their
 * average is not an integer
 * Copyright Xo Wang 2013
 * xo@geekshavefeelings.com
 * Geeks have feelings. Integer Arithmetic Continued. Blog post.
 * //https://geekshavefeelings.com/posts/integer-arithmetic-continued
 * The averaging algorithm is a generalization of the solution mentioned in
 * US-patents US08748851 (1996) and US6007232A (1998) by Roney S. Wong
 * (Samsung Electronics Co Ltd). I have no expertise in judging if the usage of
 * this code in a final product, even if non-commercial, is allowed.
 * Use at your own responsibility.
 */

static uint32_t avgNoOverflow(uint32_t /* a */, uint32_t /* b */);

/**
 * wait till we have a reading. Then: timer1CounterValue = ICR1 = TCNT1 (16 bit register, can hold maximum value
 * of 2^16=65536) overflows every (2^16) ticks = 65536 ticks = 65536 CPU clock
 * cycles (for prescaler = 1). x = y * (2^z)  x = (y << z)
 * http://www.vanguardsw.com/dphelp4/dph00340.htm). But the AVR-GCC compiler AVR-g++
 * optimizes this so we don�t have to write (overflowCopy << 16) as in
 * www.gammon.com.au/forum/bbshowpost.php?id=11504&page=1. We store the raw values
 * and do the calculation later
 */
void getAveragePulseWidth();

void storeAveratePulseWidthHighMicros1ThenSetMode2OrTimeOut(unsigned int /*timeOut*/);

void waitForPWMSustainThenSetMode3();

void reactToPulseWidthThenSetMode1(unsigned int /* pulseWidthHighMicrosA */, unsigned int /* pulseWidthHighMicrosB */);


//Do not add code below this line
#endif /* _PipetBot_A8Xtend_H_ */

