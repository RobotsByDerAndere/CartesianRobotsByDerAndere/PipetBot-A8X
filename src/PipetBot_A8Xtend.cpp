/**
 * @title: PipetBot_A8Xtend.cpp
 * @version: 6.0
 * @author: DerAndere
 * @created: 2018
 * // SPDX-License-Identifier: MIT
 * //Copyright � 2018 DerAndere
 * @license: MIT
 * @language: C++
 * @info: https://it-by-derandere.blogspot.com/programming
 * @about: This sketch can be uploaded on an Atmega328 to receive commands or
 * data encoded as Servo-PWM signals via its pin PB2 (digital pin 8 of an
 * Arduino Uno Rev.3 development board)
 * The first PWM pulse burst with a period of 20 ms should last for
 * 200 ms and encodes  the type of command. It should be followed by a 300 ms
 * burst with a period of 20 ms and a pulse width of > 19950 us high (3.3-5 mV).
 * The following burst of pulses with a period of 20 ms encodes a parameter value
 * and should last for 300 ms followed by a 300 ms burst with a period of 20 ms
 * and a pulse width of > 19950 us.
 * When no command is to be transmitted, the voltage at the pin PB2 (digital
 * pin 8 of the Arduino Uno Rev.3) should be constantly 0mV (state "low") or
 * a Servo-PWM signal with a period of 20 ms and a pulse width of either
 * < 300 us or > 19950 us.
 * Adapted from "Measuring a duty cycle using the input capture unit"
 * (http://www.gammon.com.au/forum/?id=11504&reply=13#reply13, Copyright: Nick
 * Gammon 2015, Creative Commons Attribution 3.0 Australia License. The averaging
 * algorithm is from https://geekshavefeelings.com/posts/integer-arithmetic-continued
 * https://geekshavefeelings.com/posts/integer-arithmetic-continued (Copyright
 * (c) Xo Wang 2013) with permission from Xo Wang.
 * That averaging algorithm is a generalization of the solution mentioned in
 * US-patents US08748851 (1996) and US6007232A (1998) by Roney S. Wong
 * (Samsung Electronics Co Ltd). I have no expertise in judging if the usage of
 * this code in a final product, even if non-commercial, is allowed.
 * Use at your own responsibility.
 * Adapted by Author: DerAndere
 * @language: C/C++11 for the Microchip AVR toolchain based on g++ from the
 * GNU Compiler Collection
 * @launch target: Microchip Atmega328.
 */


#include "PipetBot_A8Xtend.h"

#include <Arduino.h> // Available after installing the Arduino IDE https://www.arduino.cc/en/Main/Software
#include "ServoTimer2.h" // https://github.com/nabontra/ServoTimer2

// 16 bit timer1 overflows every 2^16 = 65536 counts. Count the overflows to calculate long time periods unsigned type for handling of rollover by unsigned integer arithmetics
ISR (TIMER1_OVF_vect)
{
	++overflowCount;
}  // end of TIMER1_OVF_vect

/**
 * Input Capture at the ICR pin (PB2 of the Atmega328, digital pin 8 of the Arduino
 * Uno Rev.3) is the best approach measuring pulses with pulse widths below 50ms. If
 * a change from 0 V at The 16 bit timer1 value in the TCNT1 register is stored in
 * the ICR1 register.
 */
ISR (TIMER1_CAPT_vect) {
/**
 * Check, pin state changed from low (0V) to high (> 0V) (rising flank detected)
 * in order to ensure that reading pulseWidths starts when input capture ISR was
 * called by an input capture interrupt when a rising flank was detected.
 * Then save timer1 counter values for both flanks for 2-3 pulses.
 */
	/**
	 * continue to record timer1 counter values at edge detection only if a
	 * measurement series was already started or start a new series only at
	 * rising edge.
	 */
	if (bit_is_set(TCCR1B, ICES1) || index > 0) {
		// if just missed an overflow, save the adjusted overflowCount
		if (bit_is_set(TIFR1,TOV1) && ICR1 < 0x7FFF) {
			overflowCountAtFlanks[index] = overflowCount + 1;
		}

		// else save overflowCount
		else {
			overflowCountAtFlanks[index] = overflowCount;
		}
/**
 * save the timer1 value for the detected flank that was temporarily saved in the
 * register ICR1. Starts with rising flank for index = 0. Then increase index for
 * capturing the next flank.
 */
		timer1CounterValueAtFlanks[index] = ICR1;
		if (index <= (2 * maxPulseReads - 1)) {
			++index;
		}
	}

	/**
	 * Toggle between rising edge detect and falling edge detect, then
	 * reenable timer1 input capture interrupt by clearing Timer/Counter1 Input
	 * Capture Flag
	 */
	TCCR1B ^= _BV(ICES1); 	// Toggle Input Capture Edge Select between rising
							// edge and falling edge detection
	TIFR1 = _BV(ICF1);		// Clear Timer/Counter1 Input Capture Flag by
							// setting the ICF1 bit to logical 1
							// re-enable timer1 input capture after toggling
							//
}  // end of TIMER1_CAPT_vect

/**
 * Overflow-proof average of two signed integers. The naive average function is
 * erroneous when the sum of the inputs overflows integer limits; this average
 * works by summing the halves of the input values and then correcting the sum
 * for rounding.
 * @param a first value, as a 32-bit (un)signed integer
 * @param b second value, as a 32-bit (un)signed integer
 * @return signed average of the two values, rounded towards zero if their
 * average is not an integer
 * Copyright Xo Wang 2013
 * xo@geekshavefeelings.com
 * Geeks have feelings. Integer Arithmetic Continued. Blog post.
 * //https://geekshavefeelings.com/posts/integer-arithmetic-continued
 * The averaging algorithm is a generalization of the solution mentioned in
 * US-patents US08748851 (1996) and US6007232A (1998) by Roney S. Wong
 * (Samsung Electronics Co Ltd). I have no expertise in judging if the usage of
 * this code in a final product, even if non-commercial, is allowed.
 * Use at your own responsibility.
 */
static int32_t avgNoOverflow(int32_t a, int32_t b) {
	// shifts divide by two, rounded towards negative infinity
	const int32_t sumHalves = (a >> 1) + (b >> 1);
	// this has error of magnitude one if both are odd
	const int32_t bothOdd = (a & b) & 1;
	// round toward zero; add one if one input is odd and sum is negative
	const int32_t roundToZero = (sumHalves < 0) & (a ^ b);

	// result is sum of halves corrected for rounding
	return sumHalves + bothOdd + roundToZero;
}

void getAveragePulseWidth() {
	/**
	 * wait until 3 pulses (6 flanks) were recorded for the current measurement
	 * series
	 */
	uint8_t oldSREG = SREG;
	cli();
	int indexCopy = index;
	SREG = oldSREG;
	if (indexCopy > (2 * maxPulseReads - 1)) {

		uint32_t timer1ValueAtFlanksCopy[2 * maxPulseReads - 1];
		uint32_t overflowCountAtFlanksCopy[2* maxPulseReads - 1];
		oldSREG = SREG;
		cli();
		for (uint8_t j = 0; j < (2 * maxPulseReads - 1); ++j) {
			timer1ValueAtFlanksCopy[j] = timer1CounterValueAtFlanks[j];
			overflowCountAtFlanksCopy[j] = overflowCountAtFlanks[j];
		}
		SREG = oldSREG;
		currentMillis = millis();
		previousMillis = currentMillis;

/**
 * timer1CounterValue = ICR1 = TCNT1 (16 bit register, can hold maximum value
 * of 2^16=65536) overflows every (2^16) ticks = 65536 ticks = 65536 CPU clock
 * cycles (for prescaler = 1). x = y * (2^z)  x = (y << z)
 * http://www.vanguardsw.com/dphelp4/dph00340.htm). But the AVR-GCC compiler AVR-g++
 * optimizes this so we don�t have to write (overflowCopy << 16) as in
 * www.gammon.com.au/forum/bbshowpost.php?id=11504&page=1. We store the raw values
 * and do the calculation later
 */
		uint32_t pulseHighOverflowDifferences[3] = {overflowCountAtFlanksCopy[1] - overflowCountAtFlanksCopy[0], overflowCountAtFlanksCopy[3] - overflowCountAtFlanksCopy[2], overflowCountAtFlanksCopy[5] - overflowCountAtFlanksCopy[4]};

		uint32_t pulseHighTimer1ValueDifferences[3] = {timer1ValueAtFlanksCopy[1] - timer1ValueAtFlanksCopy[0], timer1ValueAtFlanksCopy[3] - timer1ValueAtFlanksCopy[2], timer1ValueAtFlanksCopy[5] - timer1ValueAtFlanksCopy[4]};

		unsigned int pulseWidthsHighMicros[3] = {0};
		for (uint8_t k=0; k<3; ++k) {
			pulseWidthsHighMicros[k] = (unsigned int)(((65536000UL / (uint32_t)F_CPU) * pulseHighOverflowDifferences[k]) + ((1000UL * (uint32_t)pulseHighTimer1ValueDifferences[k]) / (uint32_t)F_CPU));
		}

		averagePulseWidthHighMicros = (unsigned int)(avgNoOverflow((int32_t)pulseWidthsHighMicros[0], (int32_t)pulseWidthsHighMicros[1]));

		if (indexCopy >= 3) {
			averagePulseWidthHighMicros = (unsigned int)(avgNoOverflow((int32_t)averagePulseWidthHighMicros, (int32_t)pulseWidthsHighMicros[2]));
		}
	}
}


void storeAveragePulseWidthHighMicros1ThenSetMode2OrTimeOut(unsigned int timeOut){
	if (averagePulseWidthHighMicros > timeOut) {
		uint8_t oldSREG = SREG;
		cli();
		overflowCount = 0;
		for (int i = 0; i < (maxPulseReads - 1); ++i) {
					timer1CounterValueAtFlanks[i] = 0;
					overflowCountAtFlanks[i] = 0;
		}
		index = 0;
		SREG = oldSREG;
		currentMillis = millis();
		previousMillis = currentMillis;
		mode = 1;
	}
	else {
		pulseWidthHighMicros1 = averagePulseWidthHighMicros;
		averagePulseWidthHighMicros = 65535U;
		mode = 2;
	}
}


void waitForPWMSustainThenSetMode3() {
	TIMSK1 &= ~(_BV(TOIE1) | _BV(ICIE1)); // Same as TIMSK1 &= ~(1U << TOIE1); TIMSK1 &= ~(1U << ICIE1); . Clear Timer/counter1 Overflow Interrupt Enable bit and clear Timer/counter1 Input Capture Interrupt Enable bit of the Timer/counter1 Interrupt Mask Register to disable Overflow Interrupt and Input Capture Interrupt
	currentMillis = millis();
	if ((currentMillis - previousMillis) > (pwmSustainMillis + 50UL)) {
		previousMillis = currentMillis;
		uint8_t oldSREG = SREG;
		cli();
		overflowCount = 0;
		for (int i = 0; i < (2*maxPulseReads - 1); ++i) {
			timer1CounterValueAtFlanks[i] = 0;
			overflowCountAtFlanks[i] = 0;

		}
		index = 0;
		TIMSK1 = _BV(TOIE1) | _BV(ICIE1); // Same as TIMSK1 = (1U << TOIE1); TIMSK1 |= (1U << ICIE1); . Set Timer/counter1 Overflow Interrupt Enable bit and set Timer/counter1 Input Capture Interrupt Enable bit of the Timer/counter1 Interrupt Mask Register to logical 1 to ensable Overflow Interrupt and Input Capture Interrupt
		mode = 3;
		SREG = oldSREG;
	}
}


void reactToPulseWidthThenSetMode1(unsigned int pulseWidthHighMicrosA, unsigned int pulseWidthHighMicrosB) {
	uint8_t oldSREG = SREG;
	cli();
	TIMSK1 &= ~(_BV(TOIE1) | _BV(ICIE1)); // Same as TIMSK1 &= ~(1U << TOIE1); TIMSK1 &= ~(1U << ICIE1); . Clear Timer/counter1 Overflow Interrupt Enable bit and clear Timer/counter1 Input Capture Interrupt Enable bit of the Timer/counter1 Interrupt Mask Register to disable Overflow Interrupt and Input Capture Interrupt
	SREG = oldSREG;
	if ((pulseWidthHighMicrosA >= 205U) && (pulseWidthHighMicrosA <= 295U)) {
		if ((pulseWidthHighMicrosB >= 710U) && (pulseWidthHighMicrosB <= (maxServoPWMhighMicros + 40U))) {
			servo0.write(pulseWidthHighMicrosB);
		}
	}

	if ((pulseWidthHighMicrosA >= 305U) && (pulseWidthHighMicrosA <= (maxServoPWMhighMicros - 50U))) {
		if ((pulseWidthHighMicrosB >= 710U) && (pulseWidthHighMicrosB <= (maxServoPWMhighMicros + 40U))) {
			servo1.write(pulseWidthHighMicrosB);
		}
	}

	currentMillis = millis();
	if ((currentMillis - previousMillis) > (2 * pwmSustainMillis + 50UL)) {
		previousMillis = currentMillis;

		uint8_t oldSREG = SREG;
		cli();
		overflowCount = 0;
		for (int i = 0; i < (maxPulseReads - 1); ++i) {
			timer1CounterValueAtFlanks[i] = 0;
			overflowCountAtFlanks[i] = 0;
		}
		index = 0;
		TIMSK1 = _BV(TOIE1) | _BV(ICIE1); // Same as TIMSK1 = (1U << TOIE1); TIMSK1 |= (1U << ICIE1); . Set Timer/counter1 Overflow Interrupt Enable bit and set Timer/counter1 Input Capture Interrupt Enable bit of the Timer/counter1 Interrupt Mask Register to logical 1 to enable Overflow Interrupt and Input Capture Interrupt
		SREG = oldSREG;
		mode = 1;
	}
}


void setup(){
	servo0.attach(servo0Pin);
	servo1.attach(servo1Pin);

	// set up for interrupts
	uint8_t oldSREG = SREG;
	cli ();
	// protected code
	// reset Timer 1
	TCCR1A = 0;

	TIFR1 = _BV(ICF1); 	// clear input capture flag by setting the ICF1 bit to
						// logical 1. Thereby we don't get a bogus interrupt
	TIFR1 = _BV(TOV1); 	// clear Overflow timer interrupt flags by setting the
						// TOV1 bit to logical 1. Thereby we don't get a bogus
						// interrupt. |= is not needed because clearing
						// each other bit does not do anything for interrupt flags
	TCNT1 = 0;          // Counter to zero
	overflowCount = 0;  // Therefore no overflows yet
	index = 0;

	// Timer 1 - counts clock pulses
	TIMSK1 = _BV(TOIE1) | _BV(ICIE1); // interrupt on Timer 1 overflow and input capture
	// start Timer 1, no prescaler
	TCCR1B = _BV(ICES1) | _BV(CS10); // Set Input Capture Edge Select bit to logical 1 to set edge selection to enable rising edge detect. No prescaler (prescaler =1). Input Capture Noise Cancellation can be activated with TCCR1B |= _BV(ICNC1)
	SREG = oldSREG;
} // end of setup

void loop(){

	if (mode == 1) {
		getAveragePulseWidth();

		storeAveragePulseWidthHighMicros1ThenSetMode2OrTimeOut(maxServoPWMhighMicros);
	}

	if (mode == 2) {
		waitForPWMSustainThenSetMode3();
	}

	if (mode == 3 ) {
		getAveragePulseWidth();
		pulseWidthHighMicros2 = averagePulseWidthHighMicros;
		reactToPulseWidthThenSetMode1(pulseWidthHighMicros1, pulseWidthHighMicros2);
	}

}   // end of loop

